@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <a href="{{ route('teams/edit', 'new') }}" class="btn btn-primary btn-block">Create New Team</a>
      <table class="table table-bordered">
        <tr>
          <th>ID</th>
          <th>Color</th>
          <th>Team Name</th>
          <th></th>
        </tr>
        @foreach($teams as $team)
        <tr>
          <td>{{ $team->id }}</td>
          <td><span style="color:{{ $team->color }};">■</span>{{$team->color}}</td>
          <td>{{ $team->name }}</td>
          <td>
            <a href="{{ route('teams/edit', $team->id) }}" class="btn btn-info">Edit</a>
                &nbsp;
                <a href="#" class="btn btn-danger" onclick="if(confirm('Are you sure you want to delete?')){event.preventDefault();document.getElementById('delete-{{ $team->id }}').submit();}">Del</a>
                <form id="delete-{{$team->id}}" action="{{ route('teams/delete', $team->id) }}" method="POST" style="display: none;">
                  {{ csrf_field() }}
                  <input type="hidden" name="id" value="{{ $team->id }}">
                </form>
            </td>
        </tr>
        @endforeach
    </table>
    </div>
  </div>
</div>
@endsection
