@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
        <div class="card-body">
          <form method="POST">
          {{ csrf_field() }}
            <div class="form-group">
              <label for="name">Name</label>
              <input type="text" class="form-control" id="name" name="name" required value="{{ $sla->name }}">
            </div>
            <div class="form-group">
              <label for="point">Point</label>
              <input type="number" class="form-control" id="point" name="point" required value="{{ $sla->point }}">
            </div>
            <div class="form-group">
              <label for="body">Description</label>
              <input type="text" class="form-control" id="body" name="body" value="{{ $sla->body }}">
            </div>
            <button type="submit" class="btn btn-primary btn-block">Submit</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
