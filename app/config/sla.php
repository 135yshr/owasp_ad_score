<?php
return [
    'penalty' => env('SLA_PENALTY', .03),
    'iterate' => env('SLA_ITERATE', 3),
    'start' => env('SLA_START', '2018-06-24 10:00:00'),
    'end' => env('SLA_END', '2018-06-25 10:00:00'),
];
