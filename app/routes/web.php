<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'TopController@index');

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/teams', 'TeamController@index')->name('teams');
Route::get('/teams/edit/{id}', 'TeamController@edit')->name('teams/edit');
Route::post('/teams/edit/{id}', 'TeamController@post_edit');
Route::post('/teams/delete/{id}', 'TeamController@delete')->name('teams/delete');

Route::get('/slas', 'SLAController@index')->name('slas');
Route::get('/slas/edit/{id}', 'SLAController@edit')->name('slas/edit');
Route::post('/slas/edit/{id}', 'SLAController@post_edit');
Route::post('/slas/delete/{id}', 'SLAController@delete')->name('slas/delete');
Route::post('/slas/toggle', 'SLAController@toggle')->name('slas/toggle');

Route::get('/slalogs', 'SLALogsController@index')->name('slalogs');
