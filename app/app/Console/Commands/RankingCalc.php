<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Config;
use App\User;
use App\Team;
use App\Sla;
use App\Slalog;
use App\Rank;

class RankingCalc extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rankingcalc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'ranking calculation.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $now = strtotime('now');
        $start = strtotime(Config::get('sla.start'));
        $end = strtotime(Config::get('sla.end'));
        if($now >= $start && $now <= $end)
        {
            $last_count = Rank::getLastRank();
            $iterate = Config::get('sla.iterate') * 60;
            $count = floor(($now - $start) / $iterate);

            if($count > $last_count)
            {
                $teams = Team::all();
                $slas = Sla::where('is_enabled', 1)->get();
                for($last_count; $count > $last_count; $last_count++)
                {
                    $last_ranks = Rank::getLastRanks();

                    $last_start = date('Y-m-d H:i:s', ($last_count - 1) * $iterate + $start);
                    $last_end = date('Y-m-d H:i:s', ($last_count) * $iterate + $start);

                    $calc_start = date('Y-m-d H:i:s', $last_count * $iterate + $start);
                    $calc_end = date('Y-m-d H:i:s', ($last_count + 1) * $iterate + $start);

                    $logs = Slalog::whereBetween('created_at', [ $calc_start, $calc_end])->get();
                    foreach($teams as $team)
                    {
                        $success_point = 0;
                        $failed_count = 0;

                        foreach($slas as $sla)
                        {
                            $result = 0;
                            foreach($logs as $log)
                            {
                                if($team->id == $log->team_id && $sla->id == $log->sla_id)
                                {
                                    $result = $log->is_success;
                                    if($result == 1)
                                    {
                                        $success_point += $sla->point;
                                    }
                                    break;
                                }
                            }
                            if($result != 1)
                            {
                                $failed_count++;
                            }
                        }

                        $score = 0;
                        if($last_count != 0)
                        {
                            foreach($last_ranks as $last_rank)
                            {
                                if($last_rank->team_id == $team->id)
                                {
                                    $score = $last_rank->score;
                                    break;
                                }
                            }
                        }

                        $score += $success_point;
                        $score -= floor($score * ($failed_count * (double)Config::get('sla.penalty')));
                        $rank = new Rank;
                        $rank->team_id = $team->id;
                        $rank->count = $last_count + 1;
                        $rank->score = $score;
                        $rank->rank = 0;
                        $rank->save();
                    }
                    Rank::Calc($last_count + 1);
                }
            }
        }
    }
}