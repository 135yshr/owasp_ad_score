$option = {
  scales: {
      xAxes: [{
          type: 'time',
          time: {
              parser: "YYYY-MM",
              unit: "minute",
              displayFormats: {
                  minute: 'h:mm a'
              },
              tooltipFormat: 'h:mm a'
          },
          ticks: {
              autoSkip: true,
          }
      }]
  }
};

function rankingChart(){

  var labels = [];
  var datasets = [];
  
  $.getJSON("/api/sla/histories", function(json){
      var ctx = document.getElementById('chart').getContext('2d');
      
      for(i in json){
          labels = [];
          var data = [];
          for(j in json[i].ranks){
              data.push(json[i].ranks[j].score);
              labels.push(new Date(json[i].ranks[j].datetime));
          }

          datasets.push({
              label: json[i].team.name,
              fill: false,
              lineTension: 0,
              borderColor: json[i].team.color,
              backgroundColor: json[i].team.color,
              data: data,
          });            
      }
      
      var chart = new Chart(ctx, {
          type: 'line',
          data: {
              labels: labels,
              datasets: datasets
          },
          options: $option
      });
  });
}

function ranking(){
    $.getJSON("/api/sla/ranking/", function(json){
        $('#ranking').empty();
        var html = "";
        for(i in json){
            html += '<tr><td>' + json[i].rank + '</td><td>' + json[i].name + '</td><td>' + json[i].score + '</td></tr>';
        }
        $('#ranking').html(html);
    });
}
