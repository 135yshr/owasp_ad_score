@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <table class="table table-bordered">
        <tr>
          <th>is_success</th>
          <th>Datetime</th>
          <th>Team</th>
          <th>SLA</th>
        </tr>
        @foreach($logs as $log)
        <tr>
          <td>{{ $log->is_success }}</td>
          <td>{{ $log->created_at }}</td>
          <td>{{ $log->team->name }}({{ $log->team->id }})</td>
          <td>{{ $log->sla->name }}({{ $log->sla->id }})</td>
        </tr>
        @endforeach
      </table>
      {!! $logs->render() !!}
    </div>
  </div>
</div>
@endsection
