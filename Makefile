include .env
COMPOSE = dockerfiles/docker-compose.yml
CONTAINER = php-fpm
APPNAME = owasp_ad

setup:
	@ echo "🤖  < Resolving code dependencies..."
	@ -cd $(CURDIR)/app && \
		cp -n _env .env
	@ cd $(CURDIR)/app && \
		composer install --no-dev && \
		yarn
	@ echo
	@ echo "🤖  < Setting up encryption key..."
	@ cd $(CURDIR)/app && \
		php artisan key:generate

build:
	@ echo "🤖  < Building client code..."
	@ cd $(CURDIR)/app && \
		npm run prod

build-dev:
	@ echo "🤖  < Building client code..."
	@ cd $(CURDIR)/app && \
		npm run dev

up:
	@ docker-compose -f $(COMPOSE) -p $(APPNAME) up -d --build

restart:
	@ docker-compose -f $(COMPOSE) -p $(APPNAME) restart

clean:
	@ docker-compose -f $(COMPOSE) -p $(APPNAME) stop && \
		yes | docker-compose -f $(COMPOSE) -p $(APPNAME) rm

ps:
	@ docker-compose -f $(COMPOSE) -p $(APPNAME) ps

sh:
	@ docker-compose -f $(COMPOSE) -p $(APPNAME) exec $(CONTAINER) \
		sh

mysql:
	@ docker-compose -f $(COMPOSE) -p $(APPNAME) exec mysql \
		mysql -u $(DB_USERNAME) -p$(DB_PASSWORD) -h localhost app

migrate:
	@ docker-compose -f $(COMPOSE) -p $(APPNAME) exec php-fpm \
		php artisan migrate

init:
	@ docker-compose -f $(COMPOSE) -p $(APPNAME) exec php-fpm \
		php artisan migrate:refresh --seed

cron:
	@ docker-compose -f $(COMPOSE) -p $(APPNAME) exec php-fpm \
		/usr/local/bin/php /var/www/app/artisan schedule:run
